<!DOCTYPE html>
<html>
<head>
	<title>数据字典 Dictionaries </title>
</head>
<link rel="stylesheet" type="text/css"   href="style.css" />
<style type="text/css">
 
</style>
<body>




<?php

 


// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',True);
$config = require( './config_ysy.php');
 
define('DB_TYPE' ,  $config['DB_TYPE'] ?? 'mysql');//, // 数据库类型
define('DB_PORT' ,  $config['DB_PORT'] ?? 3306);//, // 端口
define('DB_HOST' ,  $config['DB_HOST'] ?? '127.0.0.1');//, // 数据库类型
define('DB_NAME',   $config['DB_NAME'] ?? '');//, // 数据库名
define('DB_USER',   $config['DB_USER'] ?? '');//, // 用户名
define('DB_PWD' ,   $config['DB_PWD']  ?? '');//, // 密码
define('DB_PREFIX', $config['DB_PREFIX'] ?? '');//, // 后缀


$dsn = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME;
$keys = ['PRI'=>'主键','MUL'=>'普通的b-tree索引','UNI'=>'唯一索引' ];

$htmls = [];
$htmls[] = "<h1 class='pagetitle'>数据字典 Dictionaries</h1>";

try {
    $dbh = new PDO($dsn, DB_USER, DB_PWD); //初始化一个PDO对象


    $htmls[] = "<p class='pagetitle' >连接成功 Connection succeeded </p>";













	// 数据字典 -- 索引信息
	$sql="SELECT a.*,
	MAX(a.NON_UNIQUE)    NON_UNIQUE,
	MAX(a.INDEX_TYPE)    INDEX_TYPE,
	MAX(a.INDEX_COMMENT)    INDEX_COMMENT,
	GROUP_CONCAT(a.COLUMN_NAME)    COLUMN_NAME
	FROM (SELECT
	TABLE_SCHEMA,
	TABLE_NAME,
	NON_UNIQUE,
	INDEX_NAME,
	SEQ_IN_INDEX,
	COLUMN_NAME,
	INDEX_TYPE,
	CONCAT(COMMENT,INDEX_COMMENT)    INDEX_COMMENT
	FROM INFORMATION_SCHEMA.STATISTICS
	WHERE TABLE_SCHEMA = '".DB_NAME."'
	ORDER BY TABLE_SCHEMA,TABLE_NAME,INDEX_NAME,SEQ_IN_INDEX) a
	GROUP BY a.TABLE_SCHEMA,a.TABLE_NAME,a.INDEX_NAME";
	$index_info = [];
	foreach ($dbh->query($sql) as $key => $row) {
		$index_info[$row['TABLE_NAME']][] = $row;
	}
	 



$sql = "SHOW TABLES";
$tables = [];
$sql = "SELECT  *  FROM information_schema.COLUMNS WHERE  TABLE_SCHEMA='".DB_NAME."' ";
$index = 0;


function get_id($name){
	static $id =  0;
	static $TABLE_NAME =  '' ;
	if ( $TABLE_NAME == $name){
 			return $id;
	}else{
		 $TABLE_NAME = $name;
		  return ++$id;
	}
}
 
foreach ($dbh->query($sql) as $key => $row) {   
		$tables[$row['TABLE_NAME']]['id'] = get_id( $row['TABLE_NAME'] );     	
 		$tables[$row['TABLE_NAME']]['table_name'] = $row['TABLE_NAME'];
		$tables[$row['TABLE_NAME']]['table_title'] = '';
		$tables[$row['TABLE_NAME']]['lists'][] = $row;
		$tables[$row['TABLE_NAME']]['index_info'] = $index_info[$row['TABLE_NAME']] ?? [] ;		
}
//TABLE_NAME,TABLE_COMMENT 
 $sql="SELECT TABLE_NAME,TABLE_ROWS,TABLE_COMMENT FROM information_schema.TABLES WHERE table_schema='".DB_NAME."'";
foreach ($dbh->query($sql) as $key => $row) {   
			$tables[$row['TABLE_NAME']]['table_title'] = $row['TABLE_COMMENT']; 
			$tables[$row['TABLE_NAME']]['table_rows'] = $row['TABLE_ROWS']; 
			
 }

foreach ($tables as $key => $row) {	  
 
		$htmls[] = "<p class='table_name' id='table_id_".$row['id']."'>";
		$htmls[] = "<span class='table_name-id'>".$row['id']."</span>"; 
		$htmls[] = "<span class='table_name-name'>".$row['table_name'] ."</span>"; 
		$htmls[] = "<span class='table_name-title'> ".$row['table_title']."</span>"; 
		$htmls[] = "<span class='table_name-rows'> ".$row['table_rows']."条记录</span>";
		$htmls[] = "</p>"; 
		$htmls[] = "<div class='table_lists'><table width='100%' border='0' cellspacing='1' cellpadding='0' >";
		$htmls[] = "<tr>";
		$htmls[] = "<th>字段名 </th>";
		$htmls[] = "<th>数据类型 </th>";
		$htmls[] = " <th>是否为空</th>";
		$htmls[] = " <th>键</th>";
		$htmls[] = " <th>默认值</th>";
		$htmls[] = " <th>其它的</th>"; 		
		$htmls[] = " <th>列注释</th>";  
		$htmls[] = " <th>字符集</th>"; 		
		$htmls[] = " <th>排序规则</th>"; 
		$htmls[] = "</tr>";	  
		foreach ($row['lists'] as $k => $r) {
				$htmls[] = "<tr>";
				$htmls[] = "<td class='COLUMN_NAME'> {$r['COLUMN_NAME']} </td>";
				$htmls[] = "<td class='COLUMN_TYPE' >  {$r['COLUMN_TYPE']  }</td>";				
				$htmls[] = "<td class='IS_NULLABLE' > {$r['IS_NULLABLE']} </td>";  		   
				$htmls[] = "<td class='COLUMN_KEY' >  ".  (isset($keys[$r['COLUMN_KEY']]) ? $keys[$r['COLUMN_KEY']] : '')  . "</td>";			
				$htmls[] = "<td class='COLUMN_DEFAULT' > {$r['COLUMN_DEFAULT']} </td>";		
				$htmls[] = "<td class='EXTRA' > {$r['EXTRA']} </td>";					
				$htmls[] = "<td class='COLUMN_COMMENT' > {$r['COLUMN_COMMENT']} </td>";
				$htmls[] = "<td class='CHARACTER_SET_NAME' > {$r['CHARACTER_SET_NAME']} </td>";
				$htmls[] = "<td class='COLLATION_NAME' > {$r['COLLATION_NAME']} </td>";
				$htmls[] = "</tr>";
		}
		$htmls[] = "</table>";
		$htmls[] = "</div>";
		
		
		$htmls[] = "<div class='table_index'><table width='100%' border='0' cellspacing='1' cellpadding='0' >";
		$htmls[] = "<tr>";
		$htmls[] = " <th class='index' >索引名</th>";
		$htmls[] = "<th class='index'>索引字段名 </th>";
		$htmls[] = "<th class='index' >索引类型 </th>";
		
		$htmls[] = " <th class='index' >唯一</th>";
		$htmls[] = " <th class='index' >索引注释</th>";  
		$htmls[] = "</tr>";	  
		foreach ($row['index_info'] as $k => $r) {			
			$htmls[] = "<tr>"; 					
			$htmls[] = "<td class='index-INDEX_NAME'> ".$r['INDEX_NAME'] ." </td>";
			$htmls[] = "<td class='index-COLUMN_NAME'> ".$r['COLUMN_NAME'] ." </td>"; 	
			$htmls[] = "<td class='index-INDEX_TYPE'> ".$r['INDEX_TYPE'] ." </td>";

			$htmls[] = "<td class='index-NON_UNIQUE'> ".$r['NON_UNIQUE'] ." </td>";
			$htmls[] = "<td class='index-INDEX_COMMENT'> ".$r['INDEX_COMMENT'] ." </td>";
			$htmls[] = "</tr>";	
		}
		$htmls[] = "</table>";
		$htmls[] = "</div>";
	
}

		


$htmls[] = "<div  id='nav' class='nav'><div  onclick='onclicknav()'  id='navbut'   >关闭导航</div><ul>";
foreach ($tables as $key => $row) {	  
$htmls[] = "<li><a href='#table_id_".$row['id']."'>";
$htmls[] = $row['id']."  ".$row['table_name'];
$htmls[] = "</a></li>";
}
$htmls[] = "</ul>";
$htmls[] = "</div>";


 $dbh = null;


 } catch (PDOException $e) {
    die ("Error!: " . $e->getMessage() . "<br/>");
}

echo join('',$htmls);

?>







<script src="nav.js">

	
</script>








</body>
</html>


