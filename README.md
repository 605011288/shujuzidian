# 数据字典

#### 介绍
数据字典,不用到处找数据字典,直接拿走不谢!
PHP+mysql,单文件程序.简单实用!

#### 软件架构
软件架构说明


#### 安装教程
1.  将本项目 https://gitee.com/605011288/shujuzidian.git 克隆到你项目访问目录下
2.   复制要目录的  example.config.php  为  config.php 后,修改成你具体的服务帐号密码, 
3.   也可以直接引用你的项目数据库帐号与密码. $config = require( './config.php');    
4.   只要填写对应的如下常量就可以正确连接数据库
define('DB_TYPE' ,  $config['DB_TYPE'] ?? 'mysql');//, // 数据库类型
define('DB_PORT' ,  $config['DB_PORT'] ?? 3306);//, // 端口
define('DB_HOST' ,  $config['DB_HOST'] ?? '127.0.0.1');//, // 数据库类型
define('DB_NAME',   $config['DB_NAME'] ?? '');//, // 数据库名
define('DB_USER',   $config['DB_USER'] ?? '');//, // 用户名
define('DB_PWD' ,   $config['DB_PWD']  ?? '');//, // 密码
define('DB_PREFIX', $config['DB_PREFIX'] ?? '');//, // 后缀
5. 测试 http(https)://你的域名/mysql/index.php 

#### 使用说明

1.  大家有什么好的建议可以提交到 open 分支上,感谢分享
2.  https://gitee.com/605011288/shujuzidian.git    open
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
